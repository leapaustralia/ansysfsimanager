# Installation Instruction 
 python setup.py install 
 
# To Run 
ansyfsimanager config.toml

# Example Config 

``` toml
[SC]
Exe = 'c:/Program Files/ANSYS Inc/v190/aisol/workbench.bat'
WorkingDir = 'SC'
InputFile = 'sc_input.sci'
StartAt = 0

[Fluent]
Exe = 'c:/Program Files/ANSYS Inc/v190/fluent/ntbin/win64/fluent.exe'
Name = 'Fluent'
Type = '3ddp'
Args  = [ ]
WorkingDir = 'fluent'
CaseName = 'fluentCaseFileName'
CPU = [36, 36]

[Ansys]
Exe = 'c:/Program Files/ANSYS Inc/v190/ansys/bin/winx64/ANSYS190.exe'
Name = 'Transient'
#Args  = ['-dis', '-mpi', 'INTELMPI', '-np', '18', '-b', '-o', 'solver.out']
Args  = ['-dis', '-np', '18', '-b', '-o', 'solver.out']
WorkingDir = 'ansys'
InputFileName = 'apdl.dat'
```


