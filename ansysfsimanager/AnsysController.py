'''
Copyright (c) 2017 Nishit Joseph
'''
import os
from subprocess import Popen

from . import logging

logger = logging.getLogger(__name__)


restart_cmds = '''
/batch
/solu
antype,4,rest,{},1,continue                   ! transient analysis restart 
nldiag,cont,iter           ! print out contact info each equilibrium iteration
SCOPT,NO                   ! Allow negative coefficients due to System Coupling

autots,off                 ! User turned off automatic time stepping
deltim,1.,1.,1.
time,1000.
timint,on                  ! Turn on time integration effects

outres,erase
outres,all,none
outres,nsol,all
outres,rsol,all
outres,strs,all
outres,epel,all
outres,eppl,all
outres,v,all
outres,a,all
stabilize,off                   ! Stabilization turned OFF by user

solve
fini
'''


class AnsysController(object):
    '''
    Control fluent on Windows
    '''
    def __init__(self, exe, cmds, wd, inp_name, sc):
        self._exe = exe
        self._cmds = cmds
        self._working_dir = wd
        self._process = None
        self._inp_name = inp_name
        self._sc_system = sc
        return

    def _create_input(self, restart_at=0):
        if restart_at > 0:
            inp_file_name = 'restart.dat'
            with open(os.path.join(self._working_dir, inp_file_name), 'wt') as inp_file:
                inp_file.write(restart_cmds.format(restart_at))
        else:
            inp_file_name = self._inp_name

        return inp_file_name

    def start(self):
        self._start()
        return

    def restart(self, time_step):
        logger.info('Restarting at: %s', time_step)
        self._start(time_step)
        return

    def _base_cmds(self):
        return [
            self._exe, ] + self._cmds + [
                '-schost',
                self._sc_system.host,
                '-scport',
                '{}'.format(self._sc_system.port),
                '-scname',
                '"{}"'.format(self._sc_system.ansys_system)]

    def _start(self, time_step=0):
        cmds = self._base_cmds() + ['-i', self._create_input(time_step)]
        logger.debug('Ansys Start cmds: %s', ' '.join(cmds))
        self._process = Popen(
            cmds,
            cwd=self._working_dir)
        return
