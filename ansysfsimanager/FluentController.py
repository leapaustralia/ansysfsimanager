'''
Copyright (c) 2017 Nishit Joseph
'''
import os
from subprocess import Popen

from . import logging

logger = logging.getLogger(__name__)


class FluentController(object):
    '''
    Control fluent on Windows
    '''
    def __init__(self, exe, fluent_type, wd, name, sch, sc):
        self._exe = exe
        self._fluent_type = fluent_type
        self._working_dir = wd
        self._process = None
        self._case_name = name
        self._scheme = sch
        self._sc_system = sc
        return

    def _create_input_journal(self, restart_at):
        if restart_at > 0:
            cases = [x for x in os.listdir(self._working_dir) if '.cas' in x]
            case_names = {}
            for case in cases:
                try:
                    step_no = int(case.split('.cas')[0].split('-')[-1])
                    case_names[step_no] = case
                except ValueError:
                    pass
                except Exception:
                    raise
            latest = -1
            if len(case_names) > 0 and restart_at in case_names.keys():
                latest = restart_at
            if restart_at != latest:
                raise Exception('SC is asking for a restart that is not the same as fluent')
            case_name = case_names[latest]
            jou_file_name = 'restart.jou'
        else:
            case_name = self._case_name
            jou_file_name = 'initial.jou'

        fluent_jou = self._scheme + [
            'file set-batch-options , yes , ',
            'file start-trans "Solution.trn" yes',
            'file r-c-d {}'.format(case_name),
            'file hdf-file yes',
            '(sc-solve)',
            'exit ok']
        with open(os.path.join(self._working_dir, jou_file_name), 'wt') as jou_file:
            jou_file.writelines([x + '\n' for x in fluent_jou])
        return jou_file_name

    def _base_cmds(self):
        return [
            self._exe,
            self._fluent_type,
            '-schost=' + self._sc_system.host,
            '-scport={}'.format(self._sc_system.port),
            '-scname={0}'.format(self._sc_system.fluent_system)]

    def start(self, cores):
        self._start(cores)
        return

    def restart(self, cores, time_step):
        logger.info('Restarting at: %s', time_step)
        self._start(cores, time_step)
        return

    def _start(self, cores, time_step=0):
        input_name = self._create_input_journal(time_step)
        logger.debug('Input Name: %s', input_name)
        cmds = self._base_cmds() + [
            '-t{}'.format(cores),
            '-i',
            input_name]
        logger.debug('Fluent Start cmds: %s', ' '.join(cmds))
        self._process = Popen(
            cmds,
            cwd=self._working_dir,
            shell=True)
        return
