'''
Copyright (c) 2017 Nishit Joseph
'''
import os
from subprocess import Popen, TimeoutExpired
import time

import psutil

from . import logging

logger = logging.getLogger(__name__)

class ScController(object):
    '''
    Control fluent on Windows
    '''
    def __init__(self, exe, wd, name, fluent, ansys):
        self._working_dir = wd
        self._input_name = name
        self._process = None
        self._cmds = [
            exe,
            '-cmd',
            'Ansys.Services.SystemCoupling.exe']

        self._sc_file = os.path.join(self._working_dir, 'scServer.scs')

        if os.path.exists(self._sc_file):
            os.remove(self._sc_file)

        self._fluent_key = fluent
        self._ansys_key = ansys

        self._fluent_name = ''
        self._ansys_name = ''

        self._host = ''
        self._port = -1
        return

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def fluent_system(self):
        return self._fluent_name

    @property
    def ansys_system(self):
        return self._ansys_name


    def _get_result_file(self):
        cases = [ x for x in os.listdir(self._working_dir) if '.scr' in x and 'crash' not in x and '.scr_' not in x]
        restart_files = {}
        for case in cases:
            try:
                step_no = int(case.split('.scr')[0].split('_')[-1])
                restart_files[step_no] = case
            except ValueError:
                logger.debug('Value Error: %s', case)
                pass
            except:
                raise

        if len(restart_files) == 0:
            raise Exception('Unable to restart')

        time_step = max(restart_files.keys())
        
        return time_step, restart_files[time_step]

    def _update_system(self):
        while not os.path.exists(self._sc_file):
            time.sleep(1)

        self._update_proc()

        with open(self._sc_file, 'rt') as f:
            self._port, self._host = f.readline().replace('\n', '').split('@')
            self._port = int(self._port)
            num_systems = int(f.readline().replace('\n', ''))
            part_systems = {}
            for i in range(num_systems):
                sol_label = f.readline().replace('\n', '')
                sol_name = f.readline().replace('\n', '')
                logger.debug('Label: %s  Name: %s', sol_label, sol_name)
                if self._fluent_key in sol_name:
                    self._fluent_name = sol_label
                elif self._ansys_key in sol_name:
                    self._ansys_name = sol_label
                else:
                    raise Exception('Unable to determin system type')
        logger.info(
            'SC Host: %s Port: %s Ansys: %s Fluent: %s',
            self._host,
            self._port,
            self._ansys_name,
            self._fluent_name)
        return

    def _update_proc(self):
        for proc in psutil.process_iter():
            name = proc.name()
            if 'Ansys.Services.SystemCoupling' in name:
                logger.info('Identified SC process: %s', proc) 
                self._process = proc
        return
   

    def start(self):
        cmds = self._cmds + ['-inputFile', '{}'.format(self._input_name)]
        logger.debug('SC Start cmds: %s', ' '.join(cmds))
        try:
            Popen(
            cmds,
            cwd=self._working_dir)
        except TimeoutExpired:
            pass
        except:
            raise Exception('Unable to launch SC')
        self._update_system()        
        return
    
    def restart(self):
        if os.path.exists(self._sc_file):
            os.remove(self._sc_file)

        time_step, file_name = self._get_result_file()
        cmds = self._cmds + [
            '-inputFile',
            '{}'.format(self._input_name),
            '-resultFile',
            '{}'.format(file_name)]

        logger.debug('SC Start cmds: %s', ' '.join(cmds))

        self._process = Popen(
            cmds,
            cwd=self._working_dir,
            shell=True)

        self._update_system()
        return time_step


    def wait(self):
        psutil.wait_procs([self._process,])
        cases = [
            os.path.join(self._working_dir, x)
            for x in os.listdir(self._working_dir)
            if '.scr' in x and 'crash' in x]
    
        if len(cases) > 0:
            failed = True
        else:
            failed = False
        for case in cases:
            os.remove(case)

        return failed
    
    
