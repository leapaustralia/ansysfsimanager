import logging
logging.basicConfig(
        format='%(asctime)s - %(levelname)s ' + \
        '- %(name)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M',
        level=logging.INFO)

