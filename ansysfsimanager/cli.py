'''
'''
import argparse
import toml
import time
import os
#import multiprocessing as mp
import xml.etree.ElementTree as ET

from .AnsysController import AnsysController
from .FluentController import FluentController
from .ScController import ScController
from . import logging

logger = logging.getLogger(__name__)


exit_intrupt_file = r'C:\temp\ansysfsimanager-fluent.txt'


def _get_create_args():
    '''
    Build application arguments
    '''
    parser = argparse.ArgumentParser(
        prog='ansysfsicontroller',
        description='ANSYS Bug overcome')
    parser.add_argument(
        '-r',
        '--restart',
        action='store_true',
        help='Restart analysis')

    parser.add_argument(
        'config_file',
        metavar='example.toml',
        help='Toml config file with case settings')
    return parser.parse_args()


def interrupt_fluent(working_dir, freq, sleep_time):
    logger.info('Starting Fluent Monitor!')
    if os.path.exists(exit_intrupt_file):
        os.remove(exit_intrupt_file)
    actioned_case = -1
    while True:
        if os.path.exists(exit_intrupt_file):
            logger.info('Terminiating fluent mon')
            time.sleep(2)
            os.remove(exit_intrupt_file)
            break
        temp = [x for x in os.listdir(working_dir) if '.cas.h5' in x]
        logger.debug('Cas files: %s', temp)
        cases = len(temp)
        if cases > 0 and cases != actioned_case and cases % freq == 0:
            actioned_case = cases
            logger.info('Terminiating fluent!')
            with open(r'C:\temp\exit-fluent.txt', 'wt') as exit_file:
                exit_file.write(' ')
        time.sleep(sleep_time)
    return


def update_sc_time(sc_file, count):
    tree = ET.parse(sc_file)
    delta_t = float(tree.getroot().find('Analysis').find('Step').find('Size').text)
    tree.getroot().find('Analysis').find('Duration').find('Time').text = '{0}'.format(count * delta_t)
    tree.write(sc_file)
    return


def main():
    args = _get_create_args()
    config = toml.load(args.config_file)

    logger.debug('Config: %s %s', config['SC']['WorkingDir'], config['SC']['InputFile'])
    sys_coupl = ScController(
        config['SC']['Exe'],
        config['SC']['WorkingDir'],
        config['SC']['InputFile'],
        config['Fluent']['Name'],
        config['Ansys']['Name'])

    sc_file_inp = os.path.join(
        config['SC']['WorkingDir'],
        config['SC']['InputFile'])

    if 'SchemeFile' in config['Fluent']:
        with open(os.path.join(config['Fluent']['WorkingDir'], config['Fluent']['SchemeFile']), 'rt') as sch_file:
            scheme = [x.replace('\n', '') for x in sch_file.readlines()]
    else:
        scheme = []

    fluent_sys = FluentController(
        config['Fluent']['Exe'],
        config['Fluent']['Type'],
        config['Fluent']['WorkingDir'],
        config['Fluent']['CaseName'],
        scheme,
        sys_coupl)

    ansys_sys = AnsysController(
        config['Ansys']['Exe'],
        config['Ansys']['Args'],
        config['Ansys']['WorkingDir'],
        config['Ansys']['InputFileName'],
        sys_coupl)

    fluent_cores = config['Fluent']['CPU']

    count_delta = config['SC']['Increment']
    count_end = config['SC']['StartAt'] + count_delta
    count_max = config['SC']['Max']

    update_sc_time(sc_file_inp, count_end)

    if args.restart:
        time_step = sys_coupl.restart()
        fluent_sys.restart(fluent_cores[0], time_step)
        ansys_sys.restart(time_step)
    else:
        sys_coupl.start()
        fluent_sys.start(fluent_cores[0])
        ansys_sys.start()

    fluent_cores.reverse()
    while True:
        sys_coupl.wait()
        count_end = count_end + count_delta
        if count_end > count_max:
            break
        logger.info('Setting next restart to: %s', count_end)
        update_sc_time(sc_file_inp, count_end)
        time_step = sys_coupl.restart()
        fluent_sys.restart(fluent_cores[0], time_step)
        fluent_cores.reverse()
        time.sleep(5)
        ansys_sys.restart(time_step)

    # with open(exit_intrupt_file, 'wt') as exit_file:
    #     exit_file.write(' ')

    # fluent_intrpt_p.join()


if __name__ == '__main__':
    main()
