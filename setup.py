'''
Copyright (c) 2017 Nishit Joseph
All Rights Reserved
'''

from setuptools import setup

setup(
    name='ansysfsimanager',
    version='0.0.3',
    description='ANSYS FSI Anti-Bug',
    author='Nishit Joseph',
    author_email='nish@leapaust.com.au',
    license='Apache',
    packages=['ansysfsimanager'],
    entry_points={
        'console_scripts':['ansysfsimanager=ansysfsimanager.cli:main'],
    },
    install_requires=[
        'toml',
        'psutil'
    ],
    zip_safe=False)

